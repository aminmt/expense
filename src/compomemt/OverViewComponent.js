import React, { useState } from 'react';
import TransActionForm from './TransActionForm';

const OverViewComponent = ({ income, expense, addTrasnaction }) => {
    const [isShow, setIshow] = useState(false)
    return (
        <>
            <div className='topSection'>
                <p> Balance : {income - expense}</p>
                <button 
                className={`btn ${isShow &&'cansle'}`}
                onClick={() => setIshow(prevState => !prevState)}>{isShow ? 'cansel' : 'add'}</button>
            </div>
            {
                isShow && <TransActionForm addTrasnaction={addTrasnaction} setIshow={setIshow}/>
            }
            <div className='resultSection'>
                <div className='expenseBox'>Expense
                    <span style={{color:'red'}}>{expense} $</span>
                </div>
                <div className='expenseBox'>
                    Income
                    <span>{income} $</span> </div>
            </div>
        </>
    );
}

export default OverViewComponent;