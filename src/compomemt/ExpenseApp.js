import React, { useState,useEffect } from 'react';
import OverViewComponent from './OverViewComponent';
import TransActionsComponent from './TransActionsComponent'
const ExpenseApp = () => {
    const [expense, setExpense] = useState(0)
    const [income, setIncome] = useState(0)
    const [transsctions, setTransactions] = useState([])
    const addTrasnaction = (formValues) => {

        const data = { ...formValues, id: Date.now() }
        setTransactions([...transsctions, data])

    }
useEffect(() => {
  let exp=0
  let inc=0;
  transsctions.forEach((t)=>{
      console.log(t)
      t.type==="expense" ? (exp=exp+parseFloat(t.amount)): (inc=inc+parseFloat(t.amount))
  setExpense(exp)
  setIncome(inc)
    })
}, [transsctions])

    return (
        <section className='container'>


            <OverViewComponent income={income} expense={expense} addTrasnaction={addTrasnaction} />
            <TransActionsComponent transsctions={transsctions} />
        </section>
    );
}

export default ExpenseApp;