const TransActionsComponent = ({ transsctions }) => {
    return (
        <section>
            {transsctions.length && transsctions.map((t) => (
                <div key={t.id} className="transAction" style={{ borderRight: t.type === 'expense' && '4px solid red' }}>
                    <span>  {t.desc}</span>
                    <span>${t.amount}</span>

                </div>
            ))}
        </section>
    );
}

export default TransActionsComponent;